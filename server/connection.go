package server

import (
	"github.com/gorilla/websocket"
	"log"
)

func reader(conn *websocket.Conn, username string, out chan interface{}) {
	for {
		m := Message{}

		err := conn.ReadJSON(&m)
		if err != nil {
			if websocket.IsCloseError(err, 1000, 1006) {
				log.Printf("%s disconnected", username)
				return
			}

			log.Println(err)
			continue
		}

		m.Username = username
		m.Out = out

		log.Println(m)
	}
}

func writer(conn *websocket.Conn, out chan interface{}) {
	for {
		m, ok := <-out
		if !ok {
			return
		}

		err := conn.WriteJSON(m)
		if err != nil {
			log.Println(err)
		}
	}
}
