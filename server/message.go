package server

type Message struct {
	Username string
	Out      chan interface{}
	Type     string
	Data     map[string]interface{}
}
