package main

import (
	"github.com/gorilla/websocket"
	"log"
	"tictactoe-golang/server"
	"time"
)

func main() {
	go func() {
		time.Sleep(500 * time.Millisecond)

		conn, _, err := websocket.DefaultDialer.Dial("ws://localhost:8080?username=heszki", nil)
		if err != nil {
			log.Fatal(err)
		}

		conn.WriteJSON(map[string]interface{}{
			"type": "Subscribe",
			"Data": map[string]interface{}{
				"Channel": "Lobby",
			},
		})

		conn.Close()

		time.Sleep(500 * time.Millisecond)
	}()

	server.Run()
}
